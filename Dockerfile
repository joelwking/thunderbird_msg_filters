#
#     Copyright (c) 2024 Joel W. King
#     All rights reserved.
#
#     author: @joelwking
#     written:  17 June 2024
#     references:
#       None
#
# FROM python:3.8.10-slim-buster
FROM ubuntu:latest
LABEL maintainer="Joel W. King" email="ccie1846@gmail.com"
RUN apt update && \
    apt -y install git && \
    apt -y install python3-pip && \
    pip install --upgrade pip 
#
RUN mkdir /src
COPY requirements.txt /src
WORKDIR /src
RUN pip install -r requirements.txt
#
