# Mozilla Thunderbird Message Filters

## Description

This project is a demonstration and training exercise in reading Thunderbird Mail filter file(s) and parsing the data and reformatting it to several different outputs.

The program `parse_message_filters.py` also illustrates two forms of reading the input filter file. One method is to open a file handle, and use `readline` to reference the lines in the file.

The second example, defines a Python 'generator' function, and then iterates over the file using the generator.

The `-l` (`--logic`) switch allows the user to switch between the input methods.

## Installation

This solution uses VS Code, Docker Desktop, and Git. Refer to the public repository on how to install and configure these tools; [vs_code_remote_container](https://gitlab.com/joelwking/vs_code_remote_container/-/tree/master?ref_type=heads).

After your local development environment is configured, clone this repository `https://gitlab.com/joelwking/thunderbird_msg_filters.git`, enter the cloned directory, and start VS Code, eg. `code .`.

When prompted if you would like to re-open the directory in a container, select that prompt.

## Usage

Upload a filter file from your Thunderbird mail client into the `data` directory, then run the program while referencing that input file. The filters are stored within the profile. On MacOS, navigate to `~/Library/Thunderbird/Profiles/` using the Finder.

```shell
% python3 parse_message_filters.py -h
usage: parse_message_filters [-h] -i INPUT [-l {readline,generator}] [-y]

parses Thunderbird message filter file(s)

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Thunderbird message filter file
  -l {readline,generator}, --logic {readline,generator}
                        Determine logic to read the filter file
  -y, --yaml            Optionally dump filters in YAML

================================================================================
```

## Support

Use the `https://gitlab.com/joelwking/thunderbird_msg_filters/-/issues` tab for issues.

## Roadmap

It evolves as I wish to prove out new featues.

## Contributing

Use the `https://gitlab.com/joelwking/thunderbird_msg_filters/-/issues` tab to report any issues or suggestions for new features.

## Authors and acknowledgment

Joel W. King @joelwking

## References

 - https://towardsdatascience.com/simplifying-the-python-code-for-data-engineering-projects-95f0c41dc58a
 - https://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do-in-python

## License

GNU V3

## Project status

I am working on this project in small units of time, bit and pieces.
