#!/usr/bin/env python3
#
#      Copyright (c) 2024  Joel W. King
#      All rights reserved
#
#      author: Joel W. King GitHub/GitLab @joelwking
#
#      description: parses Thunderbird message filter file(s)
#
#      usage:
#        python parse_message_filters.py --help
#
from tabulate import tabulate
import argparse
import ruyaml as yaml


class Reformat:
    def __init__(self):
        self.filters = dict()
        self.filter_headers = []
        self.name_located = None

    def to_dictionary(self, record):

        (record, name) = self.process_record(record.strip())
        if record:
            (rkey, rvalue) = record.split('=') # create dictionary with name as key, value is a dictionary of filter definitions
            try:
                self.filters[name]
            except KeyError:
                self.filters[name] = {}
            self.filters[name][rkey] = rvalue.strip('\"')  # remove " beginning and ending
        return
    
    def process_record(self, record):
        """
            The file is a series of filters deliniated by a 'name' variable followed by additional configuration
            variables. It is a flat file, so we must apply some structure to the filters defined in the file

            There are header records at the begining of the file, save these for future reference.
        """
        if "name=" in record:
            self.name_located = record.split("name=")[1].strip('\"')
            return (record, self.name_located)
        else:
            if self.name_located:
                return (record, self.name_located)
            else:
                self.filter_headers.append(record)
        return (None, None)


def create_list_of_lists(filters, display=True, display_length=60):
    """
        From a dictionary of dictionaries, create a list of lists
        Used to pass data to the `tabulate` library
    """

    tempf = []
    for key, value in filters.items():
        temp = []
        for key, value in value.items():
            if key in ('name', 'condition'):
                temp.append(value[:display_length])
        tempf.append(temp)

    if display:
        print(tabulate(tempf))

#
#  Option 1 - READLINE
#

def read_filter_file(fpath=None):
    """
        Open the filter file, process the records of the file. Create a dictionary with the key
        being the value of 'name' and the value being a dictionary of the filter definitions.

        Remove the quotations from the beginning and trailing parts of the record.
    """

    try:
        f = open(fpath, 'r')
    except (IOError, OSError, TypeError, FileNotFoundError) as e:
        print(f'Error opening file {fpath}, {e}')
        return False

    reformat = Reformat()
    while True:
        record = f.readline()
        if record:
            reformat.to_dictionary(record)
        else:
            f.close()
            break
    return reformat.filters

#
# Option 2 - GENERATOR
#

def create_file_generator(fpath=None):
    """
        Alternate method of reading a file using YIELD, which creates a generator
    """
    try:
        with open(fpath) as f:
            for record in f:
                yield(record)
    except (IOError, OSError, TypeError) as e:
        print(f'Error opening file {fpath}, {e}')
        raise Exception(f'Error opening file {fpath}, {e}') 


def main():
    """
    """
    parser = argparse.ArgumentParser(prog='parse_message_filters', description='parses Thunderbird message filter file(s)', epilog='='*80)
    parser.add_argument('-i', '--input', dest='input', help='Thunderbird message filter file', required=True)
    parser.add_argument('-l', '--logic', dest='logic', help='Determine logic to read the filter file', default='generator', choices=['readline', 'generator'])
    parser.add_argument('-y', '--yaml', dest='yamldump', help='Optionally dump filters in YAML', action='store_true')

    args = parser.parse_args()

    if args.logic in ('readline',):
        # Logic to read the filter file
        create_list_of_lists(read_filter_file(fpath=args.input), display=True)
    else:
        #  This logic uses a generator to read the file
        reformat = Reformat()
        file_generator = create_file_generator(fpath=args.input)
        for record in file_generator:
            reformat.to_dictionary(record)
        create_list_of_lists(reformat.filters)
    
        if args.yamldump:
            print(yaml.dump(reformat.filters, default_flow_style=False))

        print(f"header lines: {reformat.filter_headers}")

    print(f"That's all folks!")
    
if __name__ == "__main__":
    main()